{
    'name': "Custom Club",

    'summary': """
        add and display “club_or” field""",

    'description': """
        Module sale must already be installed
        Add and display “club_or” field in the form view, in the Sales & Purchase tab under the Pricelist field
        Display the "Club or" field in the order form
    """,

    'author': "Adil TAGGUINE",
    'website': "https://www.archeti.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/odoo/addons/base/module/module_data.xml
    # for the full list
    'category': 'sale',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': [sale],

    # always loaded
    'data': [
        'views/inherit_res_partner.xml',
        'views/inherit_sale_order.xml',
    ],
    # only loaded in demonstration mode
    'demo': [],
    'installable': True,
    'auto_install': False
}